Popular Movies
==============

API Key
-------
The API Key should be placed at **app\src\main\res\values\configuration.xml** in the `movie_db_api_key` property.

Stage 1
-------
Your app will:

+ Present the user with a grid arrangement of movie posters upon launch.
+ Allow your user to change sort order via a setting:
	- The sort order can be by most popular or by highest-rated.
+ Allow the user to tap on a movie poster and transition to a details screen with additional information such as:
	- Original title
	- Movie poster image thumbnail
	- A plot synopsis
	- User rating
	- Release date
